export enum MimeType {
  Docx = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
  Odt = 'application/vnd.oasis.opendocument.text',
  Html = 'text/html',
  Pdf = 'application/pdf',
}
