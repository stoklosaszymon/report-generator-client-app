import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { DocumentEditorService } from './document-editor/document-editor.service';
import { TemplateService } from './template/template.service';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import { DocumentEditorContainerModule } from '@syncfusion/ej2-angular-documenteditor';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ModalService } from './document-editor/_modal/modal.service';
import { ModalComponent } from './document-editor/_modal/modal.component';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { DocumentEditorComponent } from './document-editor/document-editor.component';

import { StoreModule } from '@ngrx/store';
import { alertReducer } from './reducers/alert.reducer';
import { DocumentEditorSidebarComponent } from './document-editor/document-editor-sidebar/document-editor-sidebar.component';
import { CodemirrorModule } from '@ctrl/ngx-codemirror';
import { DataSetComponent } from './data-set/data-set.component';
import { DataSetService } from './data-set/data-set.service';
import { TemplateComponent } from './template/template.component';
import { DocumentCreatorComponent } from './document-creator/document-creator.component';
import { AlertComponent } from './alerts/alert.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AlertService } from './alerts/alert.service';
import { DataSourceComponent } from './data-source/data-source.component';
import { DataSourceService } from './data-source/data-source.service';
import { DataSourceDialogComponent } from './data-source/data-source-dialog/data-source-dialog.component';
import { DataSourceFormComponent } from './data-source/data-source-form/data-source-form.component';
import { DataSetSelectComponent } from './data-set/data-set-select/data-set-select.component';
import { MetadataDialogComponent } from './metadata-dialog/metadata-dialog.component';
import { MetadataFormComponent } from './metadata-dialog/metadata-form/metadata-form.component';
import { TemplateListComponent } from './template/template-list/template-list.component';
import { DataSetListComponent } from './data-set/data-set-list/data-set-list.component';
import { DataSetTableComponent } from './data-set/data-set-table/data-set-table.component';

@NgModule({
  declarations: [
    AppComponent,
    ModalComponent,
    DocumentEditorComponent,
    DocumentEditorSidebarComponent,
    DataSetComponent,
    TemplateComponent,
    DocumentCreatorComponent,
    AlertComponent,
    DataSourceComponent,
    DataSourceDialogComponent,
    DataSourceFormComponent,
    DataSetSelectComponent,
    MetadataDialogComponent,
    MetadataFormComponent,
    TemplateListComponent,
    DataSetListComponent,
    DataSetTableComponent,
  ],
  imports: [
    DocumentEditorContainerModule,
    PdfViewerModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MaterialModule,
    CodemirrorModule,
    StoreModule.forRoot({
      alert: alertReducer,
    }),
    NgbModule,
  ],
  providers: [
    DocumentEditorService,
    TemplateService,
    ModalService,
    DataSetService,
    AlertService,
    DataSourceService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
