import { Alert } from './../models/alert.model';
import * as AlertActions from './../actions/alert.actions';

export function alertReducer(
  state: Alert = { type: '', message: '' },
  action: AlertActions.Actions
) {
  switch (action.type) {
    case AlertActions.EMIT_ALERT:
      return action.payload;
    default:
      return state;
  }
}
