import { Component, OnInit } from '@angular/core';
import { TemplateService } from '../template.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.css'],
})
export class TemplateListComponent implements OnInit {
  constructor(
    private templateService: TemplateService,
    private router: Router
  ) {}

  templates = [];

  getTemplates() {
    this.templateService.getTemplates().then((data) => {
      this.templates = Object.keys(data).map((key) => ({
        id: key,
        name: data[key],
      }));
    });
  }

  ngOnInit(): void {
    this.getTemplates();
  }

  onEditRedirect(id) {
    this.router.navigate(['/templates/edit', id]);
  }

  onDeleteTemplate(id) {
    this.templateService.deleteTemplate(id);
  }

  onAddRedirect() {
    this.router.navigate(['/templates/add']);
  }
}
