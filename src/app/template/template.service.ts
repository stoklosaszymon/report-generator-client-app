import { Injectable } from '@angular/core';
import { AlertService } from '../alerts/alert.service';

@Injectable({
  providedIn: 'root'
})
export class TemplateService {

  constructor(private alertService: AlertService) { }

  createQueryArray(dataSets) {
    return dataSets.reduce((p, c, i) => p + `dataSets[${i}]=${c}&`, '');
  }

  addTemplate(template, templateName: string, dataSets, extension: string = 'docx') {
    return fetch(`https://localhost:44382/api/db/templates/add/from-sfdt?templateName=${templateName}&extension=${extension}&${this.createQueryArray(dataSets)}`,
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json'
        },
        body: template,
      })
      .then(response => {
        if (response.status !== 200) {
          throw new Error('Bad response from server');
        } else {
          this.alertService.emitAlert('success', 'szablon pobrany pomyslnie');
          return response.json();
        }
      })
      .catch(e => {
        this.alertService.emitAlert('danger', 'blad przy pobieraniu szablonu');
      });
  }

  migrateTemplate(template) {
    return fetch('https://localhost:5001/api/adv/migrate',
      {
        method: 'post',
        body: template
      }
    )
      .then(resp => resp.blob());
  }

  getTemplates() {
    return fetch(`https://localhost:44382/api/db/templates`)
      .then(response => response.json());
  }

  getTemplate(id) {
    return fetch(`https://localhost:44382/api/db/templates/${id}?outputFormat=sfdt`)
      .then(response => response.json());
  }

  deleteTemplate(id) {
    return fetch(`https://localhost:44382/api/db/templates/${id}`,
      {
        method: 'DELETE'
      })
      .then(response => {
        if (response.status !== 200) {
          throw new Error('Bad response from server');
        }
      })
      .then(data => {
        this.alertService.emitAlert('success', 'szablon usuniety pomyslnie');
        return data;
      })
      .catch(e => {
        this.alertService.emitAlert('danger', 'blad przy usuwaniu szablonu');
      });
  }

  getTemplateDataSets(templateId) {
    return fetch(`https://localhost:44382/api/db/templates/${templateId}/datasets`)
      .then(response => response.json());
  }

  getFilledReport(data, templateId, outputFormat) {
    return fetch(`https://localhost:44382/api/report-service/generate?templateId=${templateId}&outputFormat=${outputFormat}`,
      {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-type' : 'application/json'
        }
      }).then(response => {
        if (response.status !== 200) {
          throw new Error('Bad response from server');
        } else {
          return response.blob();
        }
      })
      .then(result => {
        this.alertService.emitAlert('success', 'raport pobrany pomyslnie');
        return result;
      })
      .catch(e => {
        this.alertService.emitAlert('danger', 'blad przy pobieraniu raportu');
      });
  }

  getFilledRaportFromJson(data, templateId, outputFormat) {
    return fetch(`https://localhost:44382/api/report-service/generate/${templateId}?outputFormat=${outputFormat}`,
      {
        method: 'POST',
        body: data
      }).then(response => {
        if (response.status !== 200) {
          throw new Error('Bad response from server');
        } else {
          return response.blob();
        }
      })
      .then(result => {
        this.alertService.emitAlert('success', 'raport pobrany pomyslnie');
        return result;
      })
      .catch(e => {
        this.alertService.emitAlert('danger', 'blad przy pobieraniu raportu');
      });
  }

  updateTemplate(template, templateId, templateName, dataSets, extension: string = 'docx') {
    return fetch(`https://localhost:44382/api/templates/update/from-sfdt?templateId=${templateId}&templateName=${templateName}&extension=${extension}&${this.createQueryArray(dataSets)}`,
      {
        method: 'PUT',
        headers: {
          'Content-type': 'application/json'
        },
        body: template,
      })
      .then(response => {
        if (response.status !== 200) {
          throw new Error('Bad response from server');
        } else {
          this.alertService.emitAlert('success', 'szablon zaktualizowano pomyslnie');
          return response.json();
        }
      })
      .catch(e => {
        this.alertService.emitAlert('danger', 'blad przy aktualizacji szablonu');
      });
  }
}
