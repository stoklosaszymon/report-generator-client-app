import { Component, OnInit, ViewChild } from '@angular/core';
import {
  ToolbarService,
  DocumentEditorContainerComponent,
} from '@syncfusion/ej2-angular-documenteditor';
import { CdkDragDrop } from '@angular/cdk/drag-drop';
import { TemplateService } from './template.service';
import { AlertService } from '../alerts/alert.service';
import { DataSetService } from '../data-set/data-set.service';
import { MatDialog } from '@angular/material/dialog';
import { MetadataDialogComponent } from '../metadata-dialog/metadata-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { DocumentEditorService } from '../document-editor/document-editor.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css'],
  providers: [ToolbarService],
})
export class TemplateComponent implements OnInit {
  constructor(
    private dataSetService: DataSetService,
    private templateService: TemplateService,
    public alertService: AlertService,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private router: Router,
    private documentEditorService: DocumentEditorService
  ) {}

  dataSetsIds = [];
  selectedDataSet;
  templateDataSets = [];
  selectedTemplateId;
  tags = [];
  templateName;

  position;
  isCreatingNew = false;

  @ViewChild('document_editor')
  public documentEditor: DocumentEditorContainerComponent;

  ngOnInit(): void {
    this.checkActiveRoute();
  }

  checkActiveRoute() {
    this.route.url.subscribe((url) => {
      const activeUrl = url.map((e) => e.path).join('/');
      if (activeUrl === 'templates/add') {
        this.isCreatingNew = true;
        this.clearClientData();
      } else if (new RegExp('templates/edit/d*').test(activeUrl)) {
        this.route.params.subscribe((params) => {
          this.selectedTemplateId = params['id'];
          this.getTemplate();
        });
      }
    });
  }
  onCreated() {
    this.documentEditor.documentEditor.height = '460';
    this.documentEditor.documentEditor.width = '1080';
  }

  onGetDataSet(dataSet) {
    this.selectedDataSet = dataSet;
  }

  onAddDataSet() {
    this.addDataSetParams(this.selectedDataSet);
  }

  getDataSetTableFields(dataSetId) {
    this.dataSetService.getDataSetFields(dataSetId).then((data) => {
      const tableName = Object.keys(data)[0];
      this.tags = [
        ...this.tags,
        ...data[tableName].map((element) => `${tableName}.${element}`),
      ];
    });
  }

  onMouseOver(event) {
    this.position = { x: event.offsetX, y: event.offsetY, extend: false };
  }

  drop(event: CdkDragDrop<string[]>) {
    const text = `{{${event.item.data}}}`;
    this.documentEditor.documentEditor.selection.select(this.position);
    this.documentEditor.documentEditor.editor.insertText(text);
  }

  addTemplate(templateName) {
    this.templateService.addTemplate(
      this.documentEditor.documentEditor.serialize(),
      templateName,
      this.dataSetsIds
    );
  }

  updateTemplate(templateName) {
    this.templateService.updateTemplate(
      this.documentEditor.documentEditor.serialize(),
      this.selectedTemplateId,
      templateName,
      this.dataSetsIds
    );
  }

  onSubmitForm(templateName) {
    if (this.selectedTemplateId && !this.isCreatingNew) {
      this.updateTemplate(templateName);
    } else {
      this.addTemplate(templateName);
    }
  }

  addDataSetParams(dataSet) {
    if (!this.dataSetsIds.includes(dataSet.id)) {
      this.dataSetsIds.push(dataSet.id);
      this.templateDataSets.push(dataSet);
      this.getDataSetTableFields(dataSet.id);
    }
  }

  getTemplate() {
    this.templateService.getTemplate(this.selectedTemplateId).then((data) => {
      this.clearClientData();
      this.getExistingTemplate(data);
    });
  }

  onDeleteTemplate() {
    this.templateService
      .deleteTemplate(this.selectedTemplateId)
      .then(() => this.router.navigate(['templates']));
  }

  getExistingTemplate(templateInfo) {
    this.getTemplateDataSets(this.selectedTemplateId);
    this.templateName = templateInfo.Name;
    this.documentEditor.documentEditor.open(templateInfo.Data);
  }

  getTemplateDataSets(templateId) {
    this.templateService.getTemplateDataSets(templateId).then((dataSets) => {
      this.dataSetsIds = Object.keys(dataSets);
      this.templateDataSets = this.dataSetsIds.map((dataSetId) => {
        return { id: dataSetId, name: dataSets[dataSetId].name };
      });
      this.dataSetsIds.forEach((dataSetId) =>
        this.getDataSetTableFields(dataSetId)
      );
    });
  }

  clearClientData() {
    this.templateDataSets = [];
    this.dataSetsIds = [];
    this.tags = [];
  }

  onRemoveDataSet(dataSetId) {
    this.dataSetsIds = this.dataSetsIds.filter((e) => e !== dataSetId);
    this.templateDataSets = this.templateDataSets.filter(
      (e) => e.id !== dataSetId
    );
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MetadataDialogComponent, {
      width: '500px',
      data: {
        disableForm: false,
        metadata: { Name: this.templateName },
      },
    });

    dialogRef.afterClosed().subscribe((templateMetadata) => {
      this.onSubmitForm(templateMetadata.name);
    });
  }

  openFromFile(files: FileList) {
    const reader = new FileReader();
    reader.readAsArrayBuffer(files.item(0));

    reader.onload = () => {
      this.documentEditorService
        .fileToSfdt(new Blob([reader.result]))
        .then((sfdt) => this.documentEditor.documentEditor.open(sfdt));
    };
  }
}
