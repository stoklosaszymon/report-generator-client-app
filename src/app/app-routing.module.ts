import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router
import { DocumentEditorComponent } from './document-editor/document-editor.component';
import { DataSetComponent } from './data-set/data-set.component';
import { TemplateComponent } from './template/template.component';
import { DocumentCreatorComponent } from './document-creator/document-creator.component';
import { DataSourceComponent } from './data-source/data-source.component';
import { TemplateListComponent } from './template/template-list/template-list.component';
import { DataSetListComponent } from './data-set/data-set-list/data-set-list.component';

const routes: Routes = [
  { path: 'doc_editor', component: DocumentEditorComponent },
  { path: 'data_sets', component: DataSetListComponent },
  { path: 'data_sets/edit/:id', component: DataSetComponent },
  { path: 'data_sets/add', component: DataSetComponent },
  { path: 'templates', component: TemplateListComponent },
  { path: 'templates/edit/:id', component: TemplateComponent },
  { path: 'templates/add', component: TemplateComponent },
  { path: 'document_creator', component: DocumentCreatorComponent },
  { path: 'data_sources', component: DataSourceComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
