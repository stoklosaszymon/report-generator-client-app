import { Alert } from './models/alert.model';

export interface AppState {
  readonly alert: Alert;
}
