import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-metadata-form',
  templateUrl: './metadata-form.component.html',
  styleUrls: ['./metadata-form.component.css'],
})
export class MetadataFormComponent implements OnInit, OnChanges {
  metadataForm: FormGroup;
  @Output() formUpdated: EventEmitter<object> = new EventEmitter();
  @Input() disableFormInput;
  @Input() initialData = { Name: '', Description: '' };

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.setUpForm();
  }

  ngOnChanges() {
    this.setUpForm();
  }

  setUpForm() {
    this.metadataForm = this.formBuilder.group({
      name: [
        { value: this.initialData.Name, disabled: this.disableFormInput },
        Validators.required,
      ],
      description: [this.initialData.Description],
    });
  }

  onSubmitForm() {
    this.formUpdated.emit(this.metadataForm.value);
  }
}
