import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-metadata-dialog',
  templateUrl: 'metadata-dialog.component.html',
  styleUrls: ['metadata-dialog.component.css'],
})
export class MetadataDialogComponent {
  constructor(
    public dialogRef: MatDialogRef<MetadataDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmitDialog(dataSource) {
    this.dialogRef.close(dataSource);
  }
}
