import { Injectable } from '@angular/core';
import { AlertService } from '../alerts/alert.service';

@Injectable({
  providedIn: 'root',
})
export class DataSetService {
  constructor(private alertService: AlertService) {}

  saveDataSet(dataSetName, dataSourceId, result) {
    return fetch(
      `https://localhost:44382/api/db/data-sets/add?dataSetName=${dataSetName}&dataSourceId=${dataSourceId}`,
      {
        method: 'POST',
        body: JSON.stringify(result),
      }
    )
      .then((response) => {
        if (response.status >= 400 && response.status < 600) {
          throw new Error('Bad response from server');
        }
        return response.json();
      })
      .then((d) => {
        this.alertService.emitAlert(
          'success',
          'zbior danych zapisany pomyslnie'
        );
      })
      .catch((e) => {
        this.alertService.emitAlert(
          'danger',
          'blad przy zapisie zbioru danych'
        );
      });
  }

  getDataSet(dataSetId) {
    return fetch(
      `https://localhost:44382/api/db/data-sets/${dataSetId}`
    ).then((response) => response.json());
  }

  getDataSets() {
    return fetch(`https://localhost:44382/api/db/data-sets`).then((response) =>
      response.json()
    );
  }

  getDataSetFields(id) {
    return fetch(
      `https://localhost:44382/api/db/data-sets/${id}/fields`
    ).then((resp) => resp.json());
  }

  updateDataSet(dataSetId, dataSetName, dataSourceId, data) {
    return fetch(
      `https://localhost:44382/api/db/data-sets/update?dataSetId=${dataSetId}&dataSetName=${dataSetName}&dataSourceId=${dataSourceId}`,
      {
        method: 'PUT',
        body: JSON.stringify(data),
      }
    )
      .then((response) => {
        if (response.status >= 400 && response.status < 600) {
          throw new Error('Bad response from server');
        }
        return response.json();
      })
      .then((d) => {
        this.alertService.emitAlert(
          'success',
          'zbior danych zaktualizowano pomyslnie'
        );
      })
      .catch((e) => {
        this.alertService.emitAlert(
          'danger',
          'blad przy aktualizacji zbioru danych'
        );
      });
  }

  deleteDataSet(dataSetId) {
    return fetch(`https://localhost:44382/api/db/data-sets/${dataSetId}`, {
      method: 'DELETE',
    })
      .then((response) => {
        if (response.status >= 400 && response.status < 600) {
          throw new Error('Bad response from server');
        }
      })
      .then((d) => {
        this.alertService.emitAlert(
          'success',
          'zbior danych usunięto pomyslnie'
        );
      })
      .catch((e) => {
        this.alertService.emitAlert(
          'danger',
          'blad przy usuwaniu zbioru danych'
        );
      });
  }
}
