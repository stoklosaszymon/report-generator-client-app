import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { FormArray, FormControl, Validators } from '@angular/forms';
import * as CodeMirror from 'codemirror';

import 'codemirror/mode/sql/sql';
import 'codemirror/addon/hint/sql-hint';
import 'codemirror/addon/hint/show-hint';
import { DataSetService } from './data-set.service';
import { DataSourceService } from '../data-source/data-source.service';
import { MatDialog } from '@angular/material/dialog';
import { MetadataDialogComponent } from '../metadata-dialog/metadata-dialog.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

enum DataTypes {
  Text = 'text',
  Number = 'number',
  Boolean = 'boolean',
  MasterDetail = 'master-detail',
  DataSet = 'dataset'
}

@Component({
  selector: 'app-data-set',
  templateUrl: './data-set.component.html',
  styleUrls: ['./data-set.component.css'],
})
export class DataSetComponent implements AfterViewInit, OnInit {
  config: any;
  sqlEditorContent: any;
  properties = [];
  propertiesFormArray: FormArray;
  defaultValuesFormArray: FormArray;
  isCreatingNew = false;
  isUpdated = false;

  @ViewChild('codeEditor') private codeEditor: any;

  selectedDataSourceId: string;
  selectedDataSetId: string;
  selectedDataSetName: string;

  dataSetMetadata: any;

  dataSources = [];
  dataSets = [{ id: 1, name: 'Test' }];

  dataTypes = [
    { type: 'Text', value: DataTypes.Text },
    { type: 'Number', value: DataTypes.Number },
    { type: 'Boolean', value: DataTypes.Boolean },
    { type: 'Master-detail', value: DataTypes.MasterDetail },
    { type: 'dataset', value: DataTypes.DataSet },
  ];


  constructor(
    private dataSetService: DataSetService,
    private dataSourceService: DataSourceService,
    private route: ActivatedRoute,
    public dialog: MatDialog,
    private router: Router
  ) {
    this.propertiesFormArray = new FormArray([], Validators.required);
    this.defaultValuesFormArray = new FormArray([]);
  }

  ngOnInit() {
    this.configureEditor();
    this.getDataSources();
    this.route.url.subscribe((url) => {
      const activeUrl = url.map((e) => e.path).join('/');
      if (activeUrl === 'data_sets/add') {
        this.isCreatingNew = true;
      } else if (new RegExp('data_sets/edit/d*').test(activeUrl)) {
        this.route.params.subscribe((properties) => {
          this.selectedDataSetId = properties['id'];
          this.getDataSet();
        });
      }
    });
  }

  ngAfterViewInit(): void {
    const editor = this.codeEditor.codeMirror;

    editor.on('inputRead', (cm: any) => {
      CodeMirror.showHint(cm);
    });
  }

  getDataSources() {
    this.dataSourceService.getDataSources().then((json) => {
      this.dataSources = this.dataSourceService.mapDataSourcesFromJson(json);
    });
  }

  getDataSet() {
    this.dataSetService.getDataSet(this.selectedDataSetId).then((data) => {
      this.getExistingDataSet(data);
    });
  }

  getExistingDataSet(dataSetInfo: any) {
    this.selectedDataSetName = dataSetInfo.Name;
    this.sqlEditorContent = dataSetInfo.SqlQuery;
    this.selectedDataSourceId = dataSetInfo.DataSourceId;
    this.generatePropertiesFormArray();
    this.setEditorTableHints();
    const propertiesTypes = JSON.parse(dataSetInfo.Params).map(
      (param) => param.type
    );
    const propertiesValues = JSON.parse(dataSetInfo.Params).map(
      (param) => param.value
    );
    this.propertiesFormArray.setValue(propertiesTypes);
    this.defaultValuesFormArray.setValue(propertiesValues);
  }

  configureEditor() {
    this.config = {
      lineNumbers: true,
      mode: 'text/x-sql',
      hintOptions: {
        tables: {},
      },
    };
    this.sqlEditorContent = '// ... some code !';
  }

  detectPropertiesFromSql(sqlQuery: any) {
    const regExp = new RegExp(/(?<=:)\w+/g);
    return [...sqlQuery.matchAll(regExp)].map((el) => el[0]);
  }

  generatePropertiesFormArray() {
    this.properties = this.detectPropertiesFromSql(this.sqlEditorContent);
    this.propertiesFormArray.clear();
    this.properties.forEach(() => {
      this.propertiesFormArray.push(new FormControl());
      this.defaultValuesFormArray.push(new FormControl());
    });
  }

  setEditorTableHints() {
    this.dataSourceService
      .getDataSourceInfo(this.selectedDataSourceId)
      .then((json) => {
        this.config.hintOptions.tables = json;
      });
  }

  onDataSourceChange(dataSourceId: string) {
    this.selectedDataSourceId = dataSourceId;
    this.setEditorTableHints();
  }

  getFormsArrayResult() {
    return {
      params: JSON.stringify(
        this.properties.map((p, i) => ({
          name: p,
          type: this.propertiesFormArray.value[i],
          value: this.defaultValuesFormArray.value[i],
        }))
      ),
      query: this.sqlEditorContent,
    };
  }

  getTableData() {
    this.dataSetMetadata = {
      dataSourceId: this.selectedDataSourceId,
      inputData: this.getFormsArrayResult(),
    };
  }

  onSubmitForm(dataSetName: string) {
    const result = this.getFormsArrayResult();

    if (this.selectedDataSetId && this.isCreatingNew === false) {
      this.dataSetService
        .updateDataSet(
          this.selectedDataSetId,
          dataSetName,
          this.selectedDataSourceId,
          result
        )
        .then(() => (this.isUpdated = !this.isUpdated));
    } else {
      this.dataSetService
        .saveDataSet(dataSetName, this.selectedDataSourceId, result)
        .then(() => (this.isUpdated = !this.isUpdated));
    }
  }

  onDeleteDataSet() {
    if (this.selectedDataSetId && this.isCreatingNew === false) {
      this.dataSetService
        .deleteDataSet(this.selectedDataSetId)
        .then(() => (this.isUpdated = !this.isUpdated))
        .then(() => this.router.navigate(['data_sets']));
    }
  }

  changeCreatingMode() {
    this.isCreatingNew = !this.isCreatingNew;
  }

  onGetParamDataSet(dataSet, index){
     this.defaultValuesFormArray.controls[index].setValue(dataSet.id);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MetadataDialogComponent, {
      width: '500px',
      data: {
        disableForm: !this.isCreatingNew,
        metadata: { Name: this.selectedDataSetName },
      },
    });

    dialogRef.afterClosed().subscribe((metadata) => {
      this.onSubmitForm(metadata.name);
    });
  }
}
