import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
} from '@angular/core';
import { DataSetService } from '../data-set.service';

@Component({
  selector: 'app-data-set-select',
  templateUrl: 'data-set-select.component.html',
})
export class DataSetSelectComponent implements OnInit, OnChanges {
  dataSetId;
  dataSets = [];
  @Output() selectDataSet: EventEmitter<object> = new EventEmitter<object>();
  @Input() update;

  constructor(private dataSetService: DataSetService) {}

  ngOnInit(): void {
    this.getDataSets();
  }

  ngOnChanges() {
    console.log('updatuje');
    this.getDataSets();
  }

  getDataSets() {
    this.dataSetService
      .getDataSets()
      .then((data) => {
        this.dataSets = Object.keys(data).map((key) => {
          return { id: key, name: data[key] };
        });
      })
      .catch((e) => console.log(e));
  }

  onDataSetChange() {
    this.selectDataSet.emit(this.dataSetId);
  }
}
