import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataSetService } from '../data-set.service';

@Component({
  selector: 'app-data-set-list',
  styleUrls: ['./data-set-list.component.css'],
  templateUrl: './data-set-list.component.html',
})
export class DataSetListComponent implements OnInit {
  constructor(private dataSetService: DataSetService, private router: Router) {}

  public dataSetsList = [];

  private getDataSets() {
    this.dataSetService.getDataSets().then((response) => {
      this.dataSetsList = this.mapObjectValuesFromResponse(response);
    });
  }

  private mapObjectValuesFromResponse(response: { id: string }) {
    return Object.keys(response).map((key) => ({
      id: key,
      name: response[key],
    }));
  }

  public ngOnInit(): void {
    this.getDataSets();
  }

  public onNavigateToEdit(id: string) {
    this.router.navigate(['/data_sets/edit', id]);
  }

  public onNavigateToAdd() {
    this.router.navigate(['/data_sets/add']);
  }
}
