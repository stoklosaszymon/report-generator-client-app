import {
  Component,
  Input,
  OnInit,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { DataSourceService } from '../../data-source/data-source.service';

@Component({
  selector: 'app-data-set-table',
  templateUrl: 'data-set-table.component.html',
})
export class DataSetTableComponent implements OnInit {
  @Input()
  public set dataSetMetadata(dataSetMetadata: any) {
    this.getTableData(dataSetMetadata);
  }

  tableData = [{ message: 'Brak danych' }];

  tableDataKeys = Object.keys(this.tableData[0]);

  displayedColumns: string[] = [];

  ngOnInit() {}

  constructor(private dataSourceService: DataSourceService) {}

  getTableNames() {
    this.displayedColumns = [];
    this.tableDataKeys.map((key) => {
      this.displayedColumns.push(key);
    });
  }

  getTableData({ dataSourceId, inputData }) {
    if ( dataSourceId ){
    this.dataSourceService
      .getDataSourceTable(dataSourceId, inputData)
      .then((data) => {
        this.tableData = data;
        this.tableDataKeys = Object.keys(data[0]);
        this.getTableNames();
      });
    }
  }
}
