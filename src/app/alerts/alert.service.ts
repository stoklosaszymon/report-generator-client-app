import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import * as AlertActions from '../actions/alert.actions';

@Injectable({
  providedIn: 'root',
})
export class AlertService {
  constructor(private store: Store<AppState>) {}

  emitAlert(alertType, alertMessage) {
    this.store.dispatch(
      new AlertActions.EmitAlert({ message: alertMessage, type: alertType })
    );
  }
}
