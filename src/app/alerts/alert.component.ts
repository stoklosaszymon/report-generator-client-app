import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { AppState } from '../app.state';
import { Alert } from '../models/alert.model';

@Component({
  selector: 'app-alert',
  templateUrl: 'alert.component.html',
})
export class AlertComponent implements OnInit {
  private success = new Subject<{ type: string; message: string }>();

  staticAlertClosed = false;
  successMessage = '';
  alert: Observable<Alert>;
  alertInfo = { type: '', message: '' };

  constructor(private store: Store<AppState>) {
    this.alert = this.store.select('alert');

    this.alert.subscribe((data) => {
      this.changeSuccessMessage(data.type, data.message);
    });
  }

  ngOnInit(): void {
    setTimeout(() => (this.staticAlertClosed = true), 20000);

    this.success.subscribe((alertInfo) => (this.alertInfo = alertInfo));
    this.success
      .pipe(debounceTime(5000))
      .subscribe(() => (this.alertInfo.message = ''));
  }

  public changeSuccessMessage(type, message) {
    this.success.next({ type, message });
  }
}
