import { Injectable } from '@angular/core';
import { AlertService } from '../alerts/alert.service';

@Injectable({
  providedIn: 'root',
})
export class DataSourceService {
  constructor(private alertService: AlertService) {}

  getDataSources() {
    return fetch(
      `https://localhost:44382/api/db/data-sources`
    ).then((response) => response.json());
  }

  getDataSource(id) {
    return fetch(
      `https://localhost:44382/api/db/data-sources/${id}`
    ).then((response) => response.json());
  }

  getDataSourceInfo(id) {
    return fetch(
      `https://localhost:44382/api/db/data-sources/${id}/tables`
    ).then((response) => response.json());
  }

  addDataSource({ SourceType, SourceName }, data) {
    return fetch(
      `https://localhost:44382/api/db/data-sources/add?sourceType=${SourceType}&sourceName=${SourceName}`,
      {
        method: 'POST',
        body: JSON.stringify(data),
      }
    )
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Bad response from server');
        } else {
          this.alertService.emitAlert(
            'success',
            'Zrodlo danych zapisano pomyslnie'
          );
          return response.json();
        }
      })
      .catch((e) =>
        this.alertService.emitAlert(
          'danger',
          'Blad przy zapisise zrodla danych'
        )
      );
  }

  deleteDataSource(id) {
    return fetch(`https://localhost:44382/api/db/data-sources/${id}`, {
      method: 'DELETE',
    })
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Bad responseonse from server');
        }
        this.alertService.emitAlert(
          'success',
          'Zrodlo danych usunięto pomyslnie'
        );
      })
      .catch((e) =>
        this.alertService.emitAlert(
          'danger',
          'Blad przy usunieciu zrodla danych'
        )
      );
  }

  updateDataSource(id, { SourceType, SourceName }, data) {
    return fetch(
      `https://localhost:44382/api/db/data-sources/update?sourceType=${SourceType}&sourceName=${SourceName}&dataSourceId=${id}`,
      {
        method: 'PUT',
        body: JSON.stringify(data),
      }
    )
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Bad responseonse from server');
        }
        this.alertService.emitAlert(
          'success',
          'Zrodlo danych zaktualizowano pomyslnie'
        );
      })
      .catch((e) =>
        this.alertService.emitAlert(
          'danger',
          'Blad przy aktualizacji zrodla danych'
        )
      );
  }

  mapDataSourcesFromJson(jsonData) {
    return Object.keys(jsonData).map((key) => {
      return { id: Number(key), name: jsonData[key] };
    });
  }

  getDataSourceTable(dataSourceId, dataSet) {
    return fetch(
      `https://localhost:44382/api/db/data-sets/data-acquisition?dataSourceId=${dataSourceId}`,
      {
        method: 'POST',
        body: JSON.stringify(dataSet),
      }
    ).then((response) => response.json());
  }

  testConnection({ SourceType }, data) {
    return fetch(
      `https://localhost:44382/api/db/data-sources/test-connection?sourceType=${SourceType}`,
      {
        method: 'POST',
        body: JSON.stringify(data),
      }
    )
      .then((response) => {
        if (response.status !== 200) {
          throw new Error('Bad responseonse from server');
        } else {
          this.alertService.emitAlert(
            'success',
            'Polaczeno pomyslnie'
          );
          return response.json();
        }
      })
      .catch((e) =>
        this.alertService.emitAlert(
          'danger',
          'Blad przy probie polaczenia'
        )
      );
  }
}
