import { Component, Inject } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';

@Component({
  selector: 'app-data-source-dialog',
  templateUrl: 'data-source-dialog.component.html',
  styleUrls: ['data-source-dialog.component.css'],
})
export class DataSourceDialogComponent {
  constructor(public dialogRef: MatDialogRef<DataSourceDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSubmitDialog(dataSource) {
    this.dialogRef.close(dataSource);
  }
}
