import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DataSourceService } from '../data-source.service';

@Component({
  selector: 'app-data-source-form',
  templateUrl: './data-source-form.component.html',
  styleUrls: ['./data-source-form.component.css'],
})
export class DataSourceFormComponent implements OnInit, OnChanges {
  dataSourceForm: FormGroup;
  @Output() formUpdated: EventEmitter<object> = new EventEmitter();
  @Input() disableFormInput;
  @Input() initialData = {
    sourceName: '',
    sourceType: '',
    parameters: { Host: '', DbName: '' },
  };

  dbTypes = [
    { type: 'Postgres', value: 0 },
    { type: 'Firebird', value: 1 },
  ];

  constructor(
    private formBuilder: FormBuilder,
    private dataSourceService: DataSourceService
  ) {}

  ngOnInit(): void {
    this.setUpForm();
  }

  ngOnChanges() {
    this.setUpForm();
  }

  setUpForm() {
    this.dataSourceForm = this.formBuilder.group({
      dataSourceInfo: this.formBuilder.group({
        SourceName: [
          {
            value: this.initialData.sourceName,
            disabled: this.disableFormInput,
          },
          Validators.required,
        ],
        SourceType: [
          {
            value: this.initialData.sourceType,
            disabled: this.disableFormInput,
          },
          Validators.required,
        ],
      }),
      parameters: this.formBuilder.group({
        Host: [
          {
            value: this.initialData.parameters.Host,
            disabled: this.disableFormInput,
          },
          Validators.required,
        ],
        DbName: [
          {
            value: this.initialData.parameters.DbName,
            disabled: this.disableFormInput,
          },
          Validators.required,
        ],
      }),
      credentials: this.formBuilder.group({
        Login: [
          { value: '', disabled: this.disableFormInput },
          Validators.required,
        ],
        Password: [
          { value: '', disabled: this.disableFormInput },
          Validators.required,
        ],
      }),
    });
  }

  onSubmitForm() {
    this.formUpdated.emit(this.dataSourceForm.value);
  }

  onTestConnection() {
    const dataSource = this.dataSourceForm.value;

    this.dataSourceService
      .testConnection(dataSource.dataSourceInfo, {
        parameters: dataSource.parameters,
        credentials: dataSource.credentials,
      })
      .then((data) => console.log(data));
  }
}
