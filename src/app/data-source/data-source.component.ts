import { Component, OnInit } from '@angular/core';
import { DataSourceService } from './data-source.service';
import { DataSourceDialogComponent } from './data-source-dialog/data-source-dialog.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-data-source',
  templateUrl: './data-source.component.html',
  styleUrls: ['./data-source.component.css'],
})
export class DataSourceComponent implements OnInit {
  dataSourceForm: any;

  constructor(
    private dataSourceService: DataSourceService,
    public dialog: MatDialog
  ) {}

  dataSource;
  dataSources = [];
  dataSourceId;
  blockEditForm = true;

  ngOnInit(): void {
    this.getDataSources();
  }

  getDataSources() {
    this.dataSourceService.getDataSources().then((json) => {
      this.dataSources = this.dataSourceService.mapDataSourcesFromJson(json);
    });
  }

  onDataSourceChange() {
    this.dataSourceService
      .getDataSource(this.dataSourceId)
      .then((data) => (this.dataSource = this.mapDataSource(data)));
  }

  onDeleteDataSource() {
    this.dataSourceService
      .deleteDataSource(this.dataSource.id)
      .then(() => this.getDataSources());
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DataSourceDialogComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe((result) => {
      this.addDataSource(result);
      this.getDataSources();
    });
  }

  addDataSource(dataSource) {
    this.dataSourceService
      .addDataSource(dataSource.dataSourceInfo, {
        parameters: dataSource.parameters,
        credentials: dataSource.credentials,
      })
      .then(() => this.getDataSources());
  }

  mapDataSource(dataSource) {
    return {
      id: dataSource.Id,
      sourceName: dataSource.Name,
      sourceType: dataSource.SourceType,
      parameters: JSON.parse(dataSource.Parameters),
    };
  }

  onUpdateDataSource(dataSource) {
    this.dataSourceService
      .updateDataSource(this.dataSource.id, dataSource.dataSourceInfo, {
        parameters: dataSource.parameters,
        credentials: dataSource.credentials,
      })
      .then(() => this.getDataSources());
  }
}
