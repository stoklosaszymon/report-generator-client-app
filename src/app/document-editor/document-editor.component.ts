import { Component, OnInit, ViewChild } from '@angular/core';
import {
  DocumentEditorContainerComponent,
  ToolbarService,
} from '@syncfusion/ej2-angular-documenteditor';
import { DocumentEditorService } from './document-editor.service';
import { saveAs } from 'file-saver';
import { SafeResourceUrl } from '@angular/platform-browser';
import { TemplateService } from '../template/template.service';
import { L10n, setCulture } from '@syncfusion/ej2-base';
import pl from '../utilities/pl.json';

L10n.load(pl);

@Component({
  selector: 'app-document-editor',
  templateUrl: './document-editor.component.html',
  styleUrls: ['./document-editor.component.css'],
  providers: [ToolbarService],
})
export class DocumentEditorComponent implements OnInit {
  file: any;
  pdfSrc;
  url: SafeResourceUrl;

  @ViewChild('document_editor')
  public documentEditor: DocumentEditorContainerComponent;

  constructor(
    private docEditorService: DocumentEditorService,
    private templateService: TemplateService
  ) {}

  ngOnInit(): void {
    setCulture('pl');
  }

  renderDocumentInEditor(file) {
    this.docEditorService
      .fileToSfdt(file)
      .then((data) => this.documentEditor.documentEditor.open(data))
      .then((r) => this.onGetPDF());
  }

  onGetPDF() {
    this.docEditorService
      .sfdtToFile(this.documentEditor.documentEditor.serialize(), 'pdf')
      .then((file) => {
        this.pdfSrc = URL.createObjectURL(file);
      });
  }

  onCreated() {
    this.documentEditor.width = '800';
    this.documentEditor.documentEditor.width = '1110';
    this.documentEditor.toolbar.toolbar.width = '1100';
    this.documentEditor.documentEditor.height = '570';
    this.documentEditor.documentEditor.open(history.state.data);
  }

  onReadCustomTemplate(input) {
    const reader = new FileReader();
    reader.readAsArrayBuffer(input);

    reader.onload = () => {
      this.renderDocumentInEditor(new Blob([reader.result]));
    };
  }

  onSaveDocument(extension) {
    this.docEditorService
      .sfdtToFile(this.documentEditor.documentEditor.serialize(), extension)
      .then((blob) => {
        saveAs(blob, `raport.${extension}`);
      });
  }

  onPrint() {
    this.documentEditor.documentEditor.print();
  }

  onSaveTemplate(extension) {
    this.docEditorService
      .sfdtToFile(this.documentEditor.documentEditor.serialize())
      .then((blob) => {
        // this._templateService.addTemplate(blob, extension);
        // this.template = blob;
        // let temp = URL.createObjectURL(blob);
        // this.url = this.sanitizer.bypassSecurityTrustResourceUrl(temp)
      });
  }

  onMigrateTemplate(input) {
    const reader = new FileReader();
    reader.readAsArrayBuffer(input);

    reader.onload = () => {
      this.templateService
        .migrateTemplate(reader.result)
        .then((file) => this.renderDocumentInEditor(file));
    };
  }
}
