import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DocumentEditorService {
  constructor() {}

  fileToSfdt(file) {
    const formData: FormData = new FormData();
    formData.append('files', file);

    return fetch(
      'https://localhost:44382/api/report-service/import?extension=docx',
      {
        method: 'POST',
        body: formData,
      }
    ).then((response) => response.text());
  }

  sfdtToFile(sfdt, extension: string = 'docx') {
    return fetch(
      `https://localhost:44382/api/report-service/export?outputFormat=${extension}`,
      {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        body: sfdt,
      }
    ).then((response) => response.blob());
  }
}
