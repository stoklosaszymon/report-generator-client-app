import {
  Component,
  ViewChild,
  Input,
  EventEmitter,
  Output,
} from '@angular/core';
import { MimeType } from './../../utilities/mimeType';
import { ModalService } from '../_modal/modal.service';

@Component({
  selector: 'app-document-editor-sidebar',
  templateUrl: './document-editor-sidebar.component.html',
  styleUrls: ['./document-editor-sidebar.component.css'],
})
export class DocumentEditorSidebarComponent {
  constructor(private modalService: ModalService) {}

  @Input()
  pdfSrc: string;

  @Output()
  templateChanged: EventEmitter<File> = new EventEmitter();

  @Output()
  migrateTemplate: EventEmitter<File> = new EventEmitter();

  @Output()
  documentSaved: EventEmitter<string> = new EventEmitter();

  @Output()
  templateSaved: EventEmitter<string> = new EventEmitter();

  @Output()
  printDocument: EventEmitter<boolean> = new EventEmitter();

  @Output()
  previewDocument: EventEmitter<boolean> = new EventEmitter();

  body;
  templateExtension;
  resultExtension;

  templateExtensions = [
    { key: 'docx', value: MimeType.Docx },
    { key: 'html', value: MimeType.Html },
  ];

  fileExtensions = [
    { key: 'docx', value: 'docx' },
    { key: 'odt', value: 'odt' },
    { key: 'pdf', value: 'pdf' },
  ];

  onReadDocument(files: FileList) {
    this.templateChanged.emit(files.item(0));
  }

  onSaveDocument() {
    this.documentSaved.emit(this.resultExtension);
  }

  onSaveTemplate() {
    this.templateSaved.emit(this.templateExtension);
  }

  onMigrateTemplate(files: FileList) {
    this.migrateTemplate.emit(files.item(0));
  }

  onPrint() {
    this.printDocument.emit(true);
  }

  openModal(id: string) {
    this.previewDocument.emit(true);
    this.modalService.open(id);
  }

  closeModal(id: string) {
    this.modalService.close(id);
    URL.revokeObjectURL(this.pdfSrc);
  }
}
