import { Component, ViewEncapsulation, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None,
  providers: [],
})
export class AppComponent implements OnInit {
  title = 'report-generator-client-app';
  activeLink;
  links = [
    { name: 'Źródła danych', url: '/data_sources' },
    { name: 'Zbiory danych', url: '/data_sets' },
    { name: 'Szablony', url: '/templates' },
    { name: 'Raporty', url: '/document_creator' },
  ];

  constructor() {}

  ngOnInit() {}
}
