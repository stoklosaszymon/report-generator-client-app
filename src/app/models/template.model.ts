export interface Template {
  id: string;
  body: string;
}
