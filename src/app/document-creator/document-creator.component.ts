import { Component, OnInit } from '@angular/core';
import { TemplateService } from '../template/template.service';
import { FormArray, FormControl } from '@angular/forms';
import { saveAs } from 'file-saver';
import { Router } from '@angular/router';
import { DocumentEditorService } from '../document-editor/document-editor.service';

@Component({
  selector: 'app-document-creator',
  templateUrl: './document-creator.component.html',
  styleUrls: ['./document-creator.component.css'],
})
export class DocumentCreatorComponent implements OnInit {
  templates = [];
  template;
  paramsArray = new FormArray([]);
  params = [];
  resultJson;
  saveFileName: string;
  extension;
  JSONdata;

  bools = [
    { type: 'true', value: true },
    { type: 'false', value: false },
  ];

  extensions = [{ type: 'docx' }, { type: 'odt' }, { type: 'pdf' }];

  constructor(
    private templateService: TemplateService,
    private router: Router,
    private documentEditorService: DocumentEditorService
  ) {}

  ngOnInit(): void {
    this.onGetTemplates();
  }

  onGetTemplates() {
    this.templateService.getTemplates().then((data) => {
      this.templates = Object.keys(data).map((e) => ({ id: e, name: data[e] }));
    });
  }

  checkProps() {
    this.paramsArray.clear();
    if (this.params[0]) {
      this.params.forEach((prop) => {
        const tempForm = new FormArray([]);

        prop.params.forEach(() => {
          tempForm.push(new FormControl());
        });
        this.paramsArray.push(tempForm);
      });
    }
  }

  onGetFilledReport() {
    this.setUpJsonResult();

    this.templateService
      .getFilledReport(this.resultJson, this.template, this.extension)
      .then((blob) => {
        saveAs(blob, `${this.saveFileName}.${this.extension}`);
      });
  }

  onGetFilledReportFromJson() {
    this.templateService
      .getFilledRaportFromJson(this.JSONdata, this.template, this.extension)
      .then((blob) => {
        saveAs(blob, `${this.saveFileName}.${this.extension}`);
      });
  }

  setUpJsonResult() {
    Object.keys(this.resultJson).forEach((dataSet, dataSetIndex) => {
      this.paramsArray.value[dataSetIndex].forEach((value, valueIndex) => {
        this.resultJson[dataSet].params[valueIndex].Value = value;
      });
    });
  }

  onChangeTemplate() {
    this.templateService.getTemplateDataSets(this.template).then((data) => {
      this.resultJson = data;
      this.params = Object.keys(data)
        .map((key) => ({ id: key, params: data[key] }))
        .reduce(
          (p, c) => [
            ...p,
            { name: c.params.name, params: c.params.params, id: c.id },
          ],
          []
        );
      this.checkProps();
    });
  }

  onFileNameChange(event) {
    this.saveFileName = event.target.value;
  }

  onDocumentPreview() {
    this.setUpJsonResult();
    this.templateService
      .getFilledReport(this.resultJson, this.template, 'sfdt')
      .then((blob) => {
        blob['text']().then((sfdt) => {
          this.router.navigate(['/doc_editor'], { state: { data: sfdt } });
        });
      });
  }

  onJsonDocumentPreview() {
    this.setUpJsonResult();
    this.templateService
      .getFilledRaportFromJson(this.JSONdata, this.template, 'docx')
      .then((blob) => {
        this.documentEditorService.fileToSfdt(blob).then((sfdt) => {
          this.router.navigate(['/doc_editor'], { state: { data: sfdt } });
        });
      });
  }
}
