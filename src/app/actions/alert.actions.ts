import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Alert } from '../models/alert.model';

export const EMIT_ALERT = '[ALERT] Emit';

export class EmitAlert implements Action {
  readonly type = EMIT_ALERT;

  constructor(public payload: Alert) {}
}

export type Actions = EmitAlert;
